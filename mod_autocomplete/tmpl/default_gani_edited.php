<?php
/**
 * @copyright	Copyright (c) 2015 autocomplete. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
<style>
#s5_pos_custom_4
{
	padding-left: 9% !important;
	padding-right:10% !important;
	width: 80% !important;
}
.SPSearchGeneralBox {
    float: left !important;
    margin-bottom: 0px !important;
    width: 13%;

}
.SPSearchCityBox {
    float: left !important;
    margin-left: 1%;
}
.lbllacateme
{
	margin-top: 0px;
	margin-right: 0px;
	margin-left: 6px;
}
.locatemeform
{
	float:left;
}
.spField
{
	margin-left: 10px;
}
.professionresult {
    background-color: #2E74BB;
    /*margin-top: 3%;*/
    /*position: absolute;*/
    width: 105%;
}
.cityresult
{
	background-color: #2E74BB;
    margin-top: 3%;
    position: absolute;	
    margin-left: 15.1%
}

.professionresult li:hover{
	background-color: #1A61A8;
	color: black;
	cursor: pointer;
}
.cityresult li:hover{
	background-color: #1A61A8;
	color: black;
	cursor: pointer;
}
.professionsec
{
	width: 100%;
	position: absolute;
}
@media(min-width:320px) and (max-width:480px){
.SPSearchGeneralBox {
	width: 60%;
}	
.professionresult {
	width: 50%;
	}
}

</style>
<script>
jQuery(document).ready(function(){
	
	jQuery("#SPSearchCityBox").keyup(function(){
		jQuery(".cityresult").hide();
		if (jQuery(this).val().length >= 3)
		{

			var url = "index.php?option=com_ajax&task=autocomplete";
			jQuery.ajax({
			type: "POST",
			url: url,
			data:'keyword='+jQuery(this).val(),
			beforeSend: function(){
				jQuery("#SPSearchCityBox").css("background","#FFF");
			},
			success: function(data){
				
				jQuery(".cityresult").show();
				jQuery(".cityresult").html(data);
				jQuery("#SPSearchCityBox").css("background","#FFF");
			}
			});	
		}
	});
	jQuery("#SPSearchGeneralBox").keyup(function(){
		jQuery(".professionresult").hide();
		if (jQuery(this).val().length >= 3)
		{
			var url = "index.php?option=com_ajax&task=autocompleteprofessional";
			jQuery.ajax({
			type: "POST",
			url: url,
			data:'keyword='+jQuery(this).val(),
			beforeSend: function(){
				jQuery("#SPSearchGeneralBox").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
			success: function(data){
				
				jQuery(".professionresult").show();
				jQuery(".professionresult").html(data);
				jQuery("#SPSearchGeneralBox").css("background","#FFF");
			}
			});
		}
	});
	
});
function cityselect(x) 
{
	var orignalStr = x;
	if (x.indexOf("-") != -1)
	{
		var newarray = x.split("-"); 	
		var newstring = "";
		for (var i = 0;  i < newarray.length ; i++) {
			
			var string = newarray[i].substr(0, 1).toUpperCase() + newarray[i].substr(1);
			newstring = newstring + " " + string;
		};
	}
	else
	{
		var newstring = x.substr(0, 1).toUpperCase() + x.substr(1);
	}
	jQuery("#SPSearchCityBox").val(newstring.trim());
	
	jQuery("#field_city").val(orignalStr);
	jQuery(".cityresult").hide();
}
function professionselect(x,y) 
{
	jQuery("#SPSearchGeneralBox").val(jQuery(x).html());
	jQuery("#field_professions").val(y);
	jQuery(".professionresult").hide();
}
 function onPositionUpdate(position) {
            document.getElementById("txtlati").value = position.coords.latitude;
            document.getElementById("txtlongi").value = position.coords.longitude;
        }



// function geocodeLatLng() {
//   var geocoder = new google.maps.Geocoder;
//   var input = document.getElementById('latlng').value;
//   var latlngStr = input.split(',', 2);
//   // latlngStr[0]= "23.0458696";
//   // latlngStr[1]= "72.5686936";
//   var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
//   //  var latlng = {lat: 23.0458696, lng: 72.5686936};

//   geocoder.geocode({'location': latlng}, function(results, status) {
//     if (status === google.maps.GeocoderStatus.OK) {
//       if (results[1]) {
     
//         alert(results[1].formatted_address);
//      }
//    }
//   });
// }


</script>

  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="<?php echo JUri::root(); ?>/media/js/customjs.js"></script>
<p style="text-align: center;">Plus de 1000 professionnels de sant&eacute; qui offrent des services en fran&ccedil;ais</p>
<form action="index.php" method="post" id="spSearchForm">
<div class="SPSearchModue">
<div class="professionsec">
<input name="SPSearchGeneralBox" type="text" id="SPSearchGeneralBox" class="SPSearchGeneralBox" placeholder="Tapez une professions" autocomplete="off" />
<div class="professionresult"></div>
</div>
<div class="citysec">
<input name="city-field" type="text" id="SPSearchCityBox" class="SPSearchCityBox" autocomplete="off" placeholder="Tapez une ville"/>

<div class="cityresult"></div>
</div>
<input type="hidden" id="field_professions" name="field_professions">
<input type="hidden" id="field_city" name="field_city">
<div class="ctrl-geomap-search">
<div class="form-inline locatemeform">
<input type="text" autocomplete="off" data-autocomplete="yes" id="field_carte_google_location" placeholder="Saisir une adresse" class="spField" value="" name="field_carte_google[location]">
<label class="checkbox lbllacateme">
<div class="btn ctrl-locate-me" disabled="disabled"><i class="icon-plus"></i> Locate Me </div>
</label>
</div>

<!--  'field_carte_google[coordinates]' Output -->
<input type="hidden" value="" id="field-carte-google-coordinates" name="field_carte_google[coordinates]"><!-- 'field_carte_google[coordinates]' End -->


<div style="max-width:500px; max-height:300px; margin-top: 10px;display:none">
<div class="alert ctrl-geo-map-message hide"></div>
<div data-settings="eyJJZCI6ImZpZWxkX2NhcnRlX2dvb2dsZSIsIlNpemUiOnsiV2lkdGgiOiI1MDBweCIsImhlaWdodCI6IjMwMHB4In0sIlRleHRzIjp7Im11bHRpcGxlIjoiTWVyY2kgZGUgc1x1MDBlOWxlY3Rpb25uZXIgdW4gZGVzIGVtcGxhY2VtZW50cyBwcm9wb3NcdTAwZTlzIHN1ciBsYSBjYXJ0ZS4ifSwiU3RhcnRQb2ludCI6eyJMYXQiOiI1My4yNDI4MzY1IiwiTG9uZyI6Ii0xMjIuNDQyNzUxNyIsIk1hcmtlciI6ZmFsc2V9LCJab29tIjoiNSJ9" class="spField ctrl-geomap" id="field_carte_google_canvas" style="width:500px; height:300px"></div>
</div>
</div>
<?php
$pattern = SPRequest::string( 'settings_pattern', null, 'post' );
$startTime = microtime( true );
$ssid = str_replace( '.', '_', strtoupper( $pattern . '_' . $startTime ) );
?>
<input type="hidden" id="SP_ssid" name="ssid" value="<?php echo $ssid; ?>"/>
<input type="hidden" id="sp_search_for" name="sp_search_for" value="*">
<input type="hidden" id="SP_312d7f3999629cead472f837d99b00ff" name="312d7f3999629cead472f837d99b00ff" value="1"/>
<input name="search" type="submit" value="Lancer la recherche" id="top_button" />
<input name="sid" type="hidden" value="888" id="SP_sid" />
<input name="task" type="hidden" value="search.search" id="SP_task" />
<input name="option" type="hidden" value="com_sobipro" id="SP_option" />
<input name="Itemid" type="hidden" value="649" id="SP_Itemid" />
</div>

</form>
