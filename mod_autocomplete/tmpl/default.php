<?php
/**
 * @copyright   Copyright (c) 2015 autocomplete. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
<style>
#s5_pos_custom_4{padding-left: 9% !important;padding-right:10% !important;width: 80% !important;}
.spClassSearchCategory {float: left !important;}
.SPSearchCityBox {float: left !important; margin-left: 1%;}
.lbllacateme{margin-top: 0px;margin-right: 0px; margin-left: 6px;}
.locatemeform{float:left;}
.spField{margin-left: 10px;}
.professionresult {background-color: black; margin-top: 3%;position: absolute;width: 181px; z-index: 1024}
.professionresult ul li, .cityresult ul li{padding:5px 15px; background:#6fd6f7; border:1px solid #6fd6f7;}
.cityresult{background-color: black; margin-top:3%;  position:absolute; margin-left: 15.1%; width:181px; z-index: 1024}
.professionresult li:hover{background-color:#1A61A8;color:#fff;border:1px solid #6fd6f7;cursor: pointer;}
.cityresult li:hover{background-color:#1A61A8;color:#fff;border:1px solid #6fd6f7;cursor: pointer;}

.professionsec, .citysec{width:25%; float: left; position: relative;}
.ctrl-geomap-search{width: 25%; float: left;margin-right:23px;}
.SPSearchModue input{box-sizing: border-box; height: 40px; margin: 0 5% 10px 0; width: 95%;}
.SPSearchModue input:hover{border-color: #BCBCBC !important;}
.SPSearchModue input#field_carte_google_location{float: left; margin:0 2% 10px 0; width: 55% !important;}
.SPSearchModue label.checkbox.lbllacateme{width: 43%; margin: 0px;}
.SPSearchModue label.checkbox.lbllacateme .ctrl-locate-me{box-sizing:border-box; width: 100%; margin:0px; padding-left:0px; padding-right: 0px;height: 40px;line-height: 28px;}
.professionresult,
.cityresult{width: 95%; margin: 0px; margin-top: 40px;}
.SPSearchModue input#top_button{width:23%; margin-right: 0px;  background: #83c938;

  -webkit-border-radius: 5;
  -moz-border-radius: 5;
  border-radius: 5px;
  font-family: 'Roboto',Helvetica,Arial,Sans-Serif;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;}
.locatemeform{width: 100%;}

#field_geolocalisation_location { margin-top: 0px !important;width:100% !important; float:left !important;}
.loadinggif {
    background:white url('http://www.hsi.com.hk/HSI-Net/pages/images/en/share/ajax-loader.gif') no-repeat right center !important;
}

.SPSearchCityBox.loadinggif {
    background-color: white;
}
@media(max-width:599px){
    #s5_header_wrap{padding-bottom: 20px;}
    .s5_wrap #s5_pos_custom_4{width: 100% !important; padding: 0px !important;}
    .SPSearchModue input{box-sizing: border-box; height: 40px; margin: 10px 0 10px; width: 100%;}
    .SPSearchModue input#field_carte_google_location{float: left;margin-right: 4%; width: 50% !important;}
    .SPSearchModue label.checkbox.lbllacateme{width: 46%; margin: 0px; }
    .SPSearchModue label.checkbox.lbllacateme .ctrl-locate-me{box-sizing:border-box; width: 100%; margin:0px; padding-left:0px; padding-right: 0px;}
    .professionsec, .citysec {float: left; position: relative; width: 100%;}
    .professionresult,
    .cityresult{width: 100%; margin: 0px; margin-top: 40px;}
    .locatemeform{width: 100%;}
    .ctrl-geomap-search{width: 100%;}
    .SPSearchModue input#top_button{width:100%; margin-left: 0px;}
    #field_geolocalisation_location { margin-left: 0px;width: 100% !important;margin-top: 10px !important;}
    .locatemeform .lbllacateme{ width: 100% !important;margin-top: 10px !important; }
}
@media(min-width:600px) and (max-width:767px){
#s5_header_wrap{padding-bottom: 20px;}
    .s5_wrap #s5_pos_custom_4{width: 100% !important; padding: 0px !important;}
    .SPSearchModue input{box-sizing: border-box; height: 40px; margin: 0 0 10px; width: 100%;}
    .SPSearchModue input#top_button{width: 48%; margin-left: 26%;margin-top: 20px;}
    .SPSearchModue input#field_carte_google_location{float: left;margin-right: 4%; width: 50% !important;}
    .SPSearchModue label.checkbox.lbllacateme{width: 48%; margin: 0px; }
    .SPSearchModue label.checkbox.lbllacateme .ctrl-locate-me{box-sizing:border-box; width: 100%; margin-top:10px; padding-left:0px; padding-right: 0px;}
    .professionsec, .citysec {float: left; position: relative; width: 48%; margin-right: 4%;}
    .citysec{margin-right: 0px;}
    .professionresult,
    .cityresult{width: 100%; margin: 0px; margin-top: 40px;}
    .ctrl-geomap-search{width: 100%;}
    #field_geolocalisation_location { width: 48% !important;margin-left: 0px !important;margin-top: 10px !important;margin-right: 4%;}
}
@media(min-width:768px) and (max-width:899px){
    #s5_header_wrap{padding-bottom: 20px;}
    .SPSearchModue input{box-sizing: border-box; height: 40px; margin: 0 0 10px; width: 100%;}
    .SPSearchModue input#top_button{width: 48%; margin-top: 10px;}
    .SPSearchModue input#field_carte_google_location{float: left;margin-right: 4%; width: 50% !important;}
    .SPSearchModue label.checkbox.lbllacateme{width: 48%; margin: 0px; }
    .SPSearchModue label.checkbox.lbllacateme .ctrl-locate-me{box-sizing:border-box; width: 100%; margin-top:10px; padding-left:0px; padding-right: 0px;}
    .professionsec, .citysec {float: left; position: relative; width: 48%; margin-right: 4%;}
    .citysec{margin-right: 0px;}
    .professionresult,
    .cityresult{width: 100%; margin: 0px; margin-top: 40px;}
    .ctrl-geomap-search{width: 48%;}
    #field_geolocalisation_location { width: 100% !important;margin-left: 0px !important;margin-top: 10px !important;margin-right: 4%;}
    .locatemeform { }
}
@media(min-width:1024px) and (max-width:1280px){
      .professionsec, .citysec{width:24%; float: left; position: relative;}

    .ctrl-geomap-search{width: 24%; float: left;}

    }
@media(min-width:900px) and (max-width:1050px){
    .s5_wrap #s5_pos_custom_4{width: 90% !important; padding: 4.5% 5% !important;}
    .professionsec, .citysec{width:25%; float: left; position: relative;}

    .ctrl-geomap-search{width: 25%; float: left;}
    .SPSearchModue input{box-sizing: border-box; height: 40px; margin: 0 5% 10px 0; width: 95%;}
    .SPSearchModue input#field_carte_google_location{float: left; margin: 0 2% 0 0; width: 55% !important;}
    .SPSearchModue label.checkbox.lbllacateme{width: 43%; margin: 0px;}
    .SPSearchModue label.checkbox.lbllacateme .ctrl-locate-me{box-sizing:border-box; width: 100%; margin:0px; padding-left:0px; padding-right: 0px;}
    .professionresult,
    .cityresult{width: 95%; margin: 0px; margin-top: 40px;}
    .SPSearchModue input#top_button{width:19%; margin-right: 0px; margin-left: 1%;}
    .locatemeform{width: 100%;}
}


</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
<script>
jQuery(window).load(function(){
    //jQuery('.ctrl-locate-me').trigger("click");
})
google.maps.event.addDomListener(window, 'load', locationautocomplete);
function locationautocomplete() {

                    var input = document.getElementById('field_geolocalisation_location');
                    /*var options = {componentRestrictions: {country: 'us'}};*/

                    /*new google.maps.places.Autocomplete(input, options);*/
                    var autocomplete = new google.maps.places.Autocomplete(input);

                     google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();

                    document.getElementById('field-carte-google-coordinates').value = place.geometry.location.lat() +","+ place.geometry.location.lng();

                    //alert("This function is working!");
                    //alert(place.name);
                   // alert(place.address_components[0].long_name);

                });
                }

jQuery(document).ready(function(){




    jQuery("#SPSearchCityBox").keyup(function(){


        jQuery(".cityresult").hide();
        jQuery('#txtcity').val(1);

        if (jQuery(this).val().length >= 3)
        {

            jQuery('#SPSearchCityBox').addClass('loadinggif');
            var url = "index.php?option=com_customajax&task=getCityData&tmpl=component";
            jQuery.ajax({
            type: "POST",
            url: url,
            data:'keyword='+jQuery(this).val(),
            beforeSend: function(){
                jQuery("#SPSearchCityBox").css("background","#FFF");
            },
            success: function(data){

                jQuery(".cityresult").show();
                jQuery(".cityresult").html(data);
                jQuery("#SPSearchCityBox").css("background","#FFF");
                jQuery('#SPSearchCityBox').removeClass('loadinggif');
            }
            });


        }
    });
    jQuery("#spClassSearchCategory").keyup(function(){
        jQuery('#txtcategory').val(1);
        jQuery(".professionresult").hide();
        if (jQuery(this).val().length >= 3)
        {
            var url = "index.php?option=com_ajax&task=autocompleteprofessional";
            jQuery.ajax({
            type: "POST",
            url: url,
            data:'keyword='+jQuery(this).val(),
            beforeSend: function(){
                jQuery("#spClassSearchCategory").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){

                jQuery(".professionresult").show();
                jQuery(".professionresult").html(data);
                jQuery("#spClassSearchCategory").css("background","#FFF");
            }
            });
        }
    });

});
function cityselect(x,y)
{
    // var orignalStr = x;
    // if (x.indexOf("-") != -1)
    // {
    //  var newarray = x.split("-");
    //  var newstring = "";
    //  for (var i = 0;  i < newarray.length ; i++) {

    //      var string = newarray[i].substr(0, 1).toUpperCase() + newarray[i].substr(1);
    //      newstring = newstring + " " + string;
    //  };
    // }
    // else
    // {
    //  var newstring = x.substr(0, 1).toUpperCase() + x.substr(1);
    // }
    // jQuery("#SPSearchCityBox").val(newstring.trim());

    // jQuery("#field_city").val(orignalStr);
    // jQuery(".cityresult").hide();


    jQuery("#SPSearchCityBox").val(jQuery(x).html());
    jQuery("#field_city").val(y);
    jQuery('#txtcity').val(0);
    jQuery('#cityerror').hide();
    jQuery(".cityresult").hide();
}
function professionselect(x,y)
{
    jQuery("#spClassSearchCategory").val(jQuery(x).html());
    jQuery("#field_rubrique").val(y);
    jQuery('#txtcategory').val(0);
    jQuery('#categoryerror').hide();
    jQuery(".professionresult").hide();
}
 function onPositionUpdate(position) {
            document.getElementById("txtlati").value = position.coords.latitude;
            document.getElementById("txtlongi").value = position.coords.longitude;
        }

function checkvalidation()
{
    locationautocomplete();
    if (jQuery('#txtcategory').val() != 0)
    {
        jQuery('#categoryerror').show();
        return false;
    }

    if (jQuery('#txtcity').val() != 0)
    {
        jQuery('#cityerror').show();
        return false;
    }
    return true;
}

// function geocodeLatLng() {
//   var geocoder = new google.maps.Geocoder;
//   var input = document.getElementById('latlng').value;
//   var latlngStr = input.split(',', 2);
//   // latlngStr[0]= "23.0458696";
//   // latlngStr[1]= "72.5686936";
//   var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
//   //  var latlng = {lat: 23.0458696, lng: 72.5686936};

//   geocoder.geocode({'location': latlng}, function(results, status) {
//     if (status === google.maps.GeocoderStatus.OK) {
//       if (results[1]) {

//         alert(results[1].formatted_address);
//      }
//    }
//   });
// }


</script>




<h2 style="text-align: center;font-family: Roboto !important; color:#ffffff; ">DES ENTREPRISES FRANCOPHONES PARTOUT DANS LA PROVINCE</h2> <br />
<form action="index.php" method="post" id="spSearchForm" onsubmit="return checkvalidation();">
    <div class="alert alert-danger" role="alert" id="categoryerror" style="display:none;"><p><?php echo $params->get('categoryerrormsg'); ?></p></div>
    <div class="alert alert-danger" role="alert" id="cityerror" style="display:none;"><p><?php echo $params->get('cityerrormsg'); ?></p></div>
<input type="hidden" name="category" id="txtcategory" value="0">
<input type="hidden" name="category" id="txtcity" value="0">
<div class="SPSearchModue">
<div class="professionsec">
<!-- <input name="spClassSearchCategory" type="text" id="spClassSearchCategory" class="spClassSearchCategory" placeholder="Tapez une rubrique" autocomplete="off"  /> -->
<input type="text" id="SPSearchBox" class="input-medium" value="*" name="sp_search_for">
<div class="professionresult"></div>
</div>
<div class="citysec">
<input name="city-field" type="text" id="SPSearchCityBox" class="SPSearchCityBox" autocomplete="off" placeholder="Tapez une ville"/>

<div class="cityresult"></div>
</div>
<input type="hidden" id="field_rubrique" name="field_rubrique">
<input type="hidden" id="field_city" name="field_ville">
<div class="ctrl-geomap-search">
<div class="form-inline locatemeform">
 <input type="text" autocomplete="off" data-autocomplete="yes" id="field_geolocalisation_location"
 placeholder="Saisir une adresse" class="spField" value="" name="field_geolocalisation[location]">

<!-- <label class="checkbox lbllacateme">
<div class="btn ctrl-locate-me"><i class="icon-plus"></i> Me localiser</div>
</label> -->
</div>

<!--  'field_carte_google[coordinates]' Output -->
<input type="hidden" value="" id="field-carte-google-coordinates" name="field_geolocalisation[coordinates]"><!-- 'field_carte_google[coordinates]' End -->


<div style="max-width:500px; max-height:300px; margin-top: 10px;display:none">
<div class="alert ctrl-geo-map-message hide"></div>
<div data-settings="eyJJZCI6ImZpZWxkX2NhcnRlX2dvb2dsZSIsIlNpemUiOnsiV2lkdGgiOiI1MDBweCIsImhlaWdodCI6IjMwMHB4In0sIlRleHRzIjp7Im11bHRpcGxlIjoiTWVyY2kgZGUgc1x1MDBlOWxlY3Rpb25uZXIgdW4gZGVzIGVtcGxhY2VtZW50cyBwcm9wb3NcdTAwZTlzIHN1ciBsYSBjYXJ0ZS4ifSwiU3RhcnRQb2ludCI6eyJMYXQiOiI1My4yNDI4MzY1IiwiTG9uZyI6Ii0xMjIuNDQyNzUxNyIsIk1hcmtlciI6ZmFsc2V9LCJab29tIjoiNSJ9" class="spField ctrl-geomap" id="field_carte_google_canvas" style="width:500px; height:300px"></div>
</div>
</div>
<?php
$pattern = SPRequest::string( 'settings_pattern', null, 'post' );
$startTime = microtime( true );
$ssid = str_replace( '.', '_', strtoupper( $pattern . '_' . $startTime ) );
?>
<input type="hidden" id="SP_ssid" name="ssid" value="<?php echo $ssid; ?>"/>

<input type="hidden" value="1" name="6f6b048ae7adad98c6bc7e524ff5ba50" id="SP_6f6b048ae7adad98c6bc7e524ff5ba50">
<input name="search" type="submit" value="RECHERCHER" id="top_button" />
<input name="sid" type="hidden" value="1" id="SP_sid" />
<input name="task" type="hidden" value="search.search" id="SP_task" />
<input name="option" type="hidden" value="com_sobipro" id="SP_option" />
<input name="Itemid" type="hidden" value="236" id="SP_Itemid" />
</div>

</form>
