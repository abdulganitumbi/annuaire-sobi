<?php
/**
 * @copyright	Copyright (c) 2015 autocomplete. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;


?>
<script>
jQuery(document).ready(function(){
	jQuery("#SPSearchCityBox").keyup(function(){
		if (jQuery(this).val().length >= 3)
		{

			var url = "index.php?option=com_ajax&task=autocomplete";
			jQuery.ajax({
			type: "POST",
			url: url,
			data:'keyword='+jQuery(this).val(),
			beforeSend: function(){
				jQuery("#SPSearchCityBox").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
			success: function(data){
				
				jQuery(".cityresult").show();
				jQuery(".cityresult").html(data);
				jQuery("#SPSearchCityBox").css("background","#FFF");
			}
			});	
		}
	});
	jQuery("#SPSearchGeneralBox").keyup(function(){
		if (jQuery(this).val().length >= 3)
		{
			var url = "index.php?option=com_ajax&task=autocompleteprofessional";
			jQuery.ajax({
			type: "POST",
			url: url,
			data:'keyword='+jQuery(this).val(),
			beforeSend: function(){
				jQuery("#SPSearchGeneralBox").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
			success: function(data){
				
				jQuery(".professionresult").show();
				jQuery(".professionresult").html(data);
				jQuery("#SPSearchGeneralBox").css("background","#FFF");
			}
			});
		}
	});
	
});
function cityselect(x) 
{
	jQuery("#SPSearchCityBox").val(x);
	jQuery(".cityresult").hide();
}
function professionselect(x) 
{
	jQuery("#SPSearchGeneralBox").val(x);
	jQuery(".professionresult").hide();
}
 function onPositionUpdate(position) {
            document.getElementById("txtlati").value = position.coords.latitude;
            document.getElementById("txtlongi").value = position.coords.longitude;
        }



// function geocodeLatLng() {
//   var geocoder = new google.maps.Geocoder;
//   var input = document.getElementById('latlng').value;
//   var latlngStr = input.split(',', 2);
//   // latlngStr[0]= "23.0458696";
//   // latlngStr[1]= "72.5686936";
//   var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
//   //  var latlng = {lat: 23.0458696, lng: 72.5686936};

//   geocoder.geocode({'location': latlng}, function(results, status) {w
//     if (status === google.maps.GeocoderStatus.OK) {
//       if (results[1]) {
     
//         alert(results[1].formatted_address);
//      }
//    }
//   });
// }


</script>
<script src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=geocodeLatLng"
        async defer></script>
<p style="text-align: center;">Plus de 1000 professionnels de sant&eacute; qui offrent des services en fran&ccedil;ais</p>
<form action="index.php" method="post" id="spSearchForm">
<div class="SPSearchModue">
<input name="sp_search_for" type="text" value="Tapez une profession" id="SPSearchGeneralBox" class="SPSearchGeneralBox" /><br />
<div class="professionresult"></div>
<input name="field_city" type="text" id="SPSearchCityBox" class="SPSearchCityBox" />
<br />
<div class="cityresult"></div>
<input type="text" name="lat" id="lat"><br />
<input type="text" name="lng" id="lng">
<input type="button" name="locateme" onclick="getlocation()" id="locateme">
<input name="search" type="submit" value="Lancer la recherche" id="top_button" />
<input name="sid" type="hidden" value="888" id="SP_sid" />
<input name="task" type="hidden" value="search.search" id="SP_task" />
<input name="option" type="hidden" value="com_sobipro" id="SP_option" />
<input name="Itemid" type="hidden" value="649" id="SP_Itemid" />
</div>
<div class="ctrl-geomap-search">
<div class="form-inline">
<input type="text" autocomplete="off" data-autocomplete="yes" id="field_carte_google_location" placeholder="Saisir une adresse" class="spField" value="" name="field_carte_google[location]">
<label class="checkbox">
ou
<div class="btn ctrl-locate-me" disabled="disabled"><i class="icon-spinner icon-spin"></i> Me localiser</div>
</label>
</div>

<!--  'field_carte_google[coordinates]' Output -->
<input type="hidden" value="" id="field-carte-google-coordinates" name="field_carte_google[coordinates]"><!-- 'field_carte_google[coordinates]' End -->


<div style="max-width:500px; max-height:300px; margin-top: 10px;">
<div class="alert ctrl-geo-map-message hide"></div>
<div data-settings="eyJJZCI6ImZpZWxkX2NhcnRlX2dvb2dsZSIsIlNpemUiOnsiV2lkdGgiOiI1MDBweCIsImhlaWdodCI6IjMwMHB4In0sIlRleHRzIjp7Im11bHRpcGxlIjoiTWVyY2kgZGUgc1x1MDBlOWxlY3Rpb25uZXIgdW4gZGVzIGVtcGxhY2VtZW50cyBwcm9wb3NcdTAwZTlzIHN1ciBsYSBjYXJ0ZS4ifSwiU3RhcnRQb2ludCI6eyJMYXQiOiI1My4yNDI4MzY1IiwiTG9uZyI6Ii0xMjIuNDQyNzUxNyIsIk1hcmtlciI6ZmFsc2V9LCJab29tIjoiNSJ9" class="spField ctrl-geomap" id="field_carte_google_canvas" style="width:500px; height:300px"><div style="overflow: hidden; width: 500px; height: 300px;"><img style="width: 500px; height: 300px;" src="https://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&amp;1i1060&amp;2i2509&amp;2e1&amp;3u5&amp;4m2&amp;1u500&amp;2u300&amp;5m5&amp;1e0&amp;5sen-US&amp;6sca&amp;10b1&amp;12b1&amp;token=59745"></div></div>
</div>
</div>
</form>
