<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Customajax
 * @author     Abdulgani Tumbi <tumbiabdul@gmail.com>
 * @copyright  2016 Abdulgani Tumbi
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Customajax', JPATH_COMPONENT);
JLoader::register('CustomajaxController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Customajax');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
