<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Customajax
 * @author     Abdulgani Tumbi <tumbiabdul@gmail.com>
 * @copyright  2016 Abdulgani Tumbi
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class CustomajaxController
 *
 * @since  1.6
 */
class CustomajaxController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app   = JFactory::getApplication();
        $start = $app->input->getInt('start', 0);

        if ($start == 0)
        {
            $app->input->set('limitstart', 0);
        }

        $view = $app->input->getCmd('view', '');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}

    public function getCityData()
    {
        $app   = JFactory::getApplication();
        $input = JFactory::getApplication()->input;

        $name = $input->getHTML('keyword');

        $name = str_replace(" ","-", $name);


        // Initialiase variables.
        $db    = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Create the base select statement.
        $query->select('sKey,sValue')
            ->from($db->quoteName('#__sobipro_language'))

            ->where('sValue like ' . $db->quote('%' . $name . '%'))
            ->where('fid = ' . $db->quote(4))
            ->where('language = ' . $db->quote('fr-FR'))
            ->where('oType = "field_option"');



        // Set the query and load the result.
        $db->setQuery($query);
        $result = $db->loadObjectList();

        $html = "";

        $html.="<ul>";
        if (count($result) > 0)
        {
            foreach ($result as $key => $value)
            {

            $html.="<li class='cityli' onClick=cityselect(this,'".$value->sKey."')>".$value->sValue."</li>";
            }
        }
        else
        {
            $html.="<li>Essayez avec une autre ville ?</li>";
        }

        $html.="</ul>";


        echo $html;
        exit;
    }
}
